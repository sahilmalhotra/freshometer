from flask import Flask, jsonify, request
from flask.ext.pymongo import PyMongo
app = Flask(__name__)

links = []

@app.route('/', methods=['GET'])
def test():
    return jsonify({'message' : 'it works'})

@app.route('/lang', methods=['GET'])
def returnAll():
    return jsonify({'links' : links})


@app.route('/lang/<string:url>', methods=['GET'])
def returnOne(url):
    link = [urls for urls in links if urls['url'] == url]
    return jsonify({'urls' : link[0]})


@app.route('/lang', methods=['POST'])
def addOne():
    urls = {'url' : request.json['url']}
    links.append(urls)
    return jsonify({'url': links})

app.config['MONGO_DBNAME'] = 'connect_to_mongo'
app.config['MONGO_URI'] = 'mongodb:://pretty:printed@ds013951/connect_to_mongo'
mongo = PyMongo(app)
if __name__ == '__main__':
    app.run(debug=True, port=8080)